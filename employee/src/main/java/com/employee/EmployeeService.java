package com.employee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class EmployeeService {	

	private static final String BLOCKED = "BLOCKED";
	private static final String UN_BLOCKED = "UN_BLOCKED";
	private static final String STATUS_DESCRIPTION = "Lorem Ipsum is simply dummy text of "
			+ "the printing and typesetting industry. Lorem Ipsum has been the industry's "
			+ "standard dummy text ever since the 1500s, when an unknown printer took a galley "
			+ "of type and scrambled it to make a type specimen book. It has survived not only "
			+ "five centuries, but also the leap into electronic typesetting, remaining "
			+ "essentially unchanged. It was popularised in the 1960s with the release of "
			+ "Letraset sheets containing Lorem Ipsum passages,";
	
	private List<Employee> employeeList = new ArrayList<>(
		Arrays.asList(
				new Employee(0, "Abdelrahman Amr", UN_BLOCKED, STATUS_DESCRIPTION, "MY_FAWRY", 10000),
				new Employee(1, "Ahmed Baz", BLOCKED, STATUS_DESCRIPTION, "MY_FAWRY", 11000),
				new Employee(2, "Ahmed Ibrahim", UN_BLOCKED, STATUS_DESCRIPTION, "BANKING", 12000),
				new Employee(3, "Sabry Ragab", BLOCKED, STATUS_DESCRIPTION, "PAY_OUT", 13000),
				new Employee(4, "Mostafa EL-Gazzar", UN_BLOCKED, STATUS_DESCRIPTION, "MY_FAWRY", 60000),
				new Employee(5, "Magued Mamdouh", UN_BLOCKED, STATUS_DESCRIPTION, "PAY_OUT", 60000)
		)
	);

	public List<Employee> fetchAll() {
		return employeeList;
	}

	public Employee fetchEmployeeById(Employee employee) {
		Employee foundEmployee = employeeList.stream().filter(emp -> employee.getId() == emp.getId()).findAny().orElse(null);
		return foundEmployee;
	}

	public Employee blockEmployee(Employee employee) {
		for (Employee eachEmployee : employeeList) {
			if(eachEmployee.getId() == employee.getId()) {
				eachEmployee.setStatus(BLOCKED);
				return eachEmployee;
			}
		}
		return null;
	}
	
	public Employee unBlockEmployee(Employee employee) {
		for (Employee eachEmployee : employeeList) {
			if(eachEmployee.getId() == employee.getId()) {
				eachEmployee.setStatus(UN_BLOCKED);
				return eachEmployee;
			}
		}
		return null;
	}
	
	public List<Employee> getEmployeeWithStatus(Employee employee){
		List<Employee> employeesList = new ArrayList<Employee>();
		
		for (Employee eachEmployee : employeeList) {
			if(eachEmployee.getStatus().equals(employee.getStatus())) {
				employeesList.add(eachEmployee);
			}
		}
		return employeesList;
	}
}
