package com.employee;

public class Employee {

	private long id;
	private String name;
	private String status;
	private String statusDescription;
	private String department;
	private Integer salary;
	
	
	public Employee(long id, String name, String status, String statusDescription, String department, Integer salary) {
		super();
		this.id = id;
		this.name = name;
		this.status = status;
		this.statusDescription = statusDescription;
		this.department = department;
		this.salary = salary;
	}
	
	
	public Employee() {}

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getStatusDescription() {
		return statusDescription;
	}


	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}


	public String getDepartment() {
		return department;
	}


	public void setDepartment(String department) {
		this.department = department;
	}


	public Integer getSalary() {
		return salary;
	}


	public void setSalary(Integer salary) {
		this.salary = salary;
	}
	
	
	
}
