package com.employee;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	EmployeeService service;

	@CrossOrigin
	@GetMapping("/fetchAll")
	public List<Employee> fetchAllEmployee(){
		return service.fetchAll();
	}

	@CrossOrigin
	@PostMapping("/fetchById")
	public Employee fetchEmployeeById(@RequestBody Employee employee) {
		return service.fetchEmployeeById(employee);
	}

	@CrossOrigin
	@PostMapping("/blockEmployee")
	public Employee blockEmployee(@RequestBody Employee employee) {
		return service.blockEmployee(employee);
	}

	@CrossOrigin
	@PostMapping("/unBlockEmployee")
	public Employee unBlockEmployee(@RequestBody Employee employee) {
		return service.unBlockEmployee(employee);
	}

	@CrossOrigin
	@PostMapping("/getEmployeeByStatus")
	public List<Employee> getEmployeeByStatus(@RequestBody Employee employee){
		return service.getEmployeeWithStatus(employee);
	}
	
}
