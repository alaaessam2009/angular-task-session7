import { Component, OnInit } from '@angular/core';
import {EmployeeModel} from "../EmployeeModel";
import {EmployeeService} from "../employee.service";
import {ActivatedRoute} from "@angular/router";
import {RouterModule} from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

   employee : any   ;
   recievedEmpId : any ;
   isBlocked : boolean = false ;


  constructor(private employeeService: EmployeeService , private activeRoute : ActivatedRoute) {
  }

  ngOnInit(): void {
    this.recievedEmpId = this.activeRoute.snapshot.params["id"];
    console.log("recievedEmpId:::::::::::", this.recievedEmpId);
    this.employeeService.getEmpById(this.recievedEmpId).subscribe(response => {
      this.employee = response;
       if (this.employee.status == "BLOCKED")
           this.isBlocked = true ;
    })

    console.log("emb by id: ", this.employee);
  }

   changeEmpStatus(id: number) : void {
    if (this.isBlocked){
      this.employeeService.unblockedEmployee(id).subscribe(response => {
        this.employee = response;
        if (this.employee.status == "BLOCKED")
          this.isBlocked = true ;
        else
          this.isBlocked = false ;
      })
    } else {
      this.employeeService.blockedEmployee(id).subscribe(response => {
        this.employee = response;
        if (this.employee.status == "BLOCKED")
          this.isBlocked = true ;
        else
          this.isBlocked = false ;
      })
    }
   }

}
