import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import { BlockedComponent } from './blocked/blocked.component';
import { UnblockedComponent } from './unblocked/unblocked.component';
import { DetailsComponent } from './details/details.component';
import { ShowAllComponent } from './show-all/show-all.component';
import {EmployeeRoutingModule} from "./employee-routing/employee-routing.module";
import {RouterModule} from '@angular/router';

@NgModule( {
  declarations: [
    AppComponent,
    BlockedComponent,
    UnblockedComponent,
    DetailsComponent,
    ShowAllComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    EmployeeRoutingModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
