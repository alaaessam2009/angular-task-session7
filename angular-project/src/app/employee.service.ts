import {HttpClient, HttpHeaders} from "@angular/common/http";
import {EmployeeModel} from "./EmployeeModel";
import {Injectable} from "@angular/core";
import {Observable} from 'rxjs';
import {filter, map} from "rxjs/operators";
import {from} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  url: string = "http://localhost:8080/employee";

  constructor(private http: HttpClient) {
  }

  getAllEmployee(): Observable<EmployeeModel[]> {
    const requestHeaders = {
      'Content-Type': 'application/json'
    }
    return this.http.get<EmployeeModel[]>(this.url + "/fetchAll", {headers: new HttpHeaders(requestHeaders)});
  }


  getEmpById(id: number): Observable<EmployeeModel> {
    const requestHeaders = {
      'Content-Type': 'application/json'
    }
    let requestBody = {
      "id": +id
    }
    return this.http.post<EmployeeModel>(this.url + "/fetchById", requestBody, {headers: new HttpHeaders(requestHeaders)});
  }


  getEmployeeByStatus (empStatus : string): Observable<EmployeeModel[]>{
    const requestHeaders = {
      'Content-Type': 'application/json'
    }
    return this.http.post<EmployeeModel[]>(this.url +"/getEmployeeByStatus" , "{\"status\":" + "\""+empStatus +"\"}",  {headers: new HttpHeaders(requestHeaders)});
  }


  blockedEmployee(id: number): Observable<EmployeeModel> {
    let requestBody = {"id": +id}
    return this.http.post<EmployeeModel>(this.url + "/blockEmployee", requestBody);
  }


  unblockedEmployee(id: number): Observable<EmployeeModel> {
    let requestBody = {"id": +id}
    return this.http.post<EmployeeModel>(this.url + "/unBlockEmployee", requestBody);
  }


}
