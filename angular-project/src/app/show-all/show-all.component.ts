import { Component, OnInit } from '@angular/core';
import {EmployeeService} from "../employee.service";
import {EmployeeModel} from "../EmployeeModel";

@Component({
  selector: 'app-show-all',
  templateUrl: './show-all.component.html',
  styleUrls: ['./show-all.component.css']
})
export class ShowAllComponent implements OnInit {

  allEmployee : EmployeeModel [] = [] ;

  constructor(private employeeService: EmployeeService) {
  }

  ngOnInit(): void {
     this.employeeService.getAllEmployee().subscribe(response => {
       this.allEmployee = response;
     })
  }

}
