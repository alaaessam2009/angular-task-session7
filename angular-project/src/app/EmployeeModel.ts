export interface EmployeeModel {
  id: Number;
  name: string;
  status: string;
  statusDescription : string ;
  department: string;
  salary: Number;
}
