import { Component, OnInit } from '@angular/core';
import {EmployeeModel} from "../EmployeeModel";
import {EmployeeService} from "../employee.service";

@Component({
  selector: 'app-blocked',
  templateUrl: './blocked.component.html',
  styleUrls: ['./blocked.component.css']
})
export class BlockedComponent implements OnInit {

  emps : EmployeeModel [] = [] ;

  constructor(private employeeService: EmployeeService) {
  }

  ngOnInit(): void {
    this.loadBlockedEmployee() ;
  }

  loadBlockedEmployee(){
    this.employeeService.getEmployeeByStatus("BLOCKED").subscribe(response => {
      this.emps = response;
    })
  }

}
