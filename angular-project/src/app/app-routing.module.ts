import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EmployeeRoutingModule} from "./employee-routing/employee-routing.module";

@NgModule({
  imports: [EmployeeRoutingModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
