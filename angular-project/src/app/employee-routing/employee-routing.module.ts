import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {ShowAllComponent} from "../show-all/show-all.component";
import {BlockedComponent} from "../blocked/blocked.component";
import {UnblockedComponent} from "../unblocked/unblocked.component";
import {DetailsComponent} from "../details/details.component";

const routes: Routes = [
  {
    path: 'showAll',
    component: ShowAllComponent ,
  },
  {
    path: 'showBlocked',
    component: BlockedComponent
  },
  {
    path: 'showUnBlocked',
    component: UnblockedComponent
  },
  {
    path: 'details/:id',
    component: DetailsComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class EmployeeRoutingModule { }
