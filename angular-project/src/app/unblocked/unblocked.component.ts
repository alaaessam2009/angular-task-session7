import { Component, OnInit } from '@angular/core';
import {EmployeeModel} from "../EmployeeModel";
import {EmployeeService} from "../employee.service";

@Component({
  selector: 'app-unblocked',
  templateUrl: './unblocked.component.html',
  styleUrls: ['./unblocked.component.css']
})
export class UnblockedComponent implements OnInit {

  emps : EmployeeModel [] = [] ;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.loadUnBlockedEmployee();
  }

  loadUnBlockedEmployee(){
    this.employeeService.getEmployeeByStatus("UN_BLOCKED").subscribe(response => {
      this.emps = response;
    })
  }

}
